package creationalpatterns.fabric;
// Работа с методами выбранного типа (тип находится с помощью специализированного для этого класса) объекта

// В итоге: каждый класс отвечает за свою задачу SortFactory за распределение типов,
// а Sort за работу с полученным типом данных.
// Количество строчек для работы с объктом может быть большим. Но фабричный паттерн автоматизирует создание объектов
// выбранного типа и работы с ним очень сильно
public class Sort {
    public final SortFactory sortFactory;

    public Sort(SortFactory sortFactory) {
        this.sortFactory = sortFactory;
    }

    public void sort(SortFactory.SortTypes chosenSortType, int[] arr){
        ISort sortType = SortFactory.createSort(chosenSortType);
        //В какой-то ситуации здесь может быть больше строчек для работы с объектом
        //У ВСЕХ объектов будет одинаковый способ работы с ним
        sortType.sort(arr);
    }

}
