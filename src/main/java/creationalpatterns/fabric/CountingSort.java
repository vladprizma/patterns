package creationalpatterns.fabric;

public class CountingSort implements ISort {

    public void sort(int [] arr){
        int length = arr.length;

        int [] c = new int[length+1];
        for (int i = 0; i < length; i++) {
            c[arr[i]] = c[arr[i]] + 1;
        }

        int b = 0;
        for (int i = 0; i < length+1; i++){
            for (int j = 0; j < c[i]; j++) {
                arr[b] = i;
                b = b + 1;
            }
        }
    }

}
