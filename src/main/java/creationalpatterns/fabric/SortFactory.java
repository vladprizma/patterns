package creationalpatterns.fabric;
//Класс для вычисления типа объекта

public class SortFactory {

    public enum SortTypes{
        COUNTING,
        INSERTION,
        MERGE
    }

    public static ISort createSort(SortTypes sortType){
        ISort sort = null;

        switch (sortType) {
            case COUNTING:
                sort = new CountingSort();
                break;
            case INSERTION:
                sort = new InsertionSort();
                break;
            case MERGE:
                sort = new MergeSort();
                break;
        }

        return sort;
    }

}
