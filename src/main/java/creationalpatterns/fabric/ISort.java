package creationalpatterns.fabric;

//Обобщающий интерфейс для насл. классов которые будут в SortFactory
public interface ISort {

    void sort(int [] arr);

}
