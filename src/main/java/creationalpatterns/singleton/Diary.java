package creationalpatterns.singleton;

import java.util.ArrayList;

public class Diary {
    public static Diary diary;
    public static ArrayList<String> notes;

    private Diary(){}

    public static Diary getDiary(){
        if(Diary.diary == null){
            Diary.diary = new Diary();
            notes = new ArrayList<>();
        }
        return Diary.diary;
    }

    public String getNote(int index){

        if(index < 0 || index >= notes.size()){
            throw new IllegalArgumentException("Index ot of bounds");
        }
        return notes.get(index);
    }

    public void setNote(String note){
        notes.add(note);
    }

}
