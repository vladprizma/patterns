package creationalpatterns.builder.Builders;

import creationalpatterns.builder.Components.Genre;
import creationalpatterns.builder.Components.Speaker;
import creationalpatterns.builder.Components.Style;

public interface Builder {
    void setGenre(Genre genre);
    void setCost(int cost);
    void setDimension(int dimension);
    void setStyle(Style style);
    void setSpeaker(Speaker speaker);
    void setSubtitles(String subtitles);
}
