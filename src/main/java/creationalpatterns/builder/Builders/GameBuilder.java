package creationalpatterns.builder.Builders;

import creationalpatterns.builder.Components.Genre;
import creationalpatterns.builder.Components.Speaker;
import creationalpatterns.builder.Components.Style;
import creationalpatterns.builder.Games.Game;

public class GameBuilder implements Builder{
    private Genre genre;
    private int cost;
    private int dimension;
    private Style style;
    private Speaker speaker;
    private String subtitles;

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = subtitles;
    }

    public Game getResult(){
        return new Game(genre,cost,dimension,style,speaker,subtitles);
    }
}
