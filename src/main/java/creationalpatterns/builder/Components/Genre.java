package creationalpatterns.builder.Components;

public enum Genre {
    RPG,
    STRATEGY,
    PLATFORMER,
    SLASHER,
    SHOOTER;
}
