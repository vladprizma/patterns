package creationalpatterns.builder.Director;

import creationalpatterns.builder.Builders.Builder;
import creationalpatterns.builder.Components.Genre;
import creationalpatterns.builder.Components.Speaker;
import creationalpatterns.builder.Components.Style;

public class Director {

    public void constructShooter(Builder builder){
        builder.setGenre(Genre.SHOOTER);
        builder.setCost(699);
        builder.setDimension(3);
        builder.setStyle(Style.FUTURISTIC);
        builder.setSpeaker(Speaker.RUSSIAN);
        builder.setSubtitles("Creator - Vladislav Perevezntsev");
    }

    public void constructPlatformer(Builder builder){
        builder.setGenre(Genre.PLATFORMER);
        builder.setCost(399);
        builder.setDimension(2);
        builder.setStyle(Style.FANCY);
        builder.setSpeaker(Speaker.RUSSIAN);
        builder.setSubtitles("Creator - Vladislav Perevezntsev");
    }

}
