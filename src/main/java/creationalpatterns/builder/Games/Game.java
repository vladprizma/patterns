package creationalpatterns.builder.Games;

import creationalpatterns.builder.Components.Genre;
import creationalpatterns.builder.Components.Speaker;
import creationalpatterns.builder.Components.Style;

public class Game {
    private Genre genre;
    private int cost;
    private int dimension;
    private Style style;
    private Speaker speaker;
    private String subtitles;

    public Game(Genre genre, int cost, int dimension, Style style, Speaker speaker, String subtitles) {
        if(genre != null && cost > 0 && style != null && speaker != null) {
            this.genre = genre;
            this.cost = cost;
            this.dimension = dimension;
            this.style = style;
            this.speaker = speaker;
            this.subtitles = subtitles;
        }
        //throw new GameNotFoundException();
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }

    public String getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = subtitles;
    }
}
