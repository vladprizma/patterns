package creationalpatterns.prototype;

import java.util.Objects;

public class ElectronicMusic extends Music {
    public boolean computer;
    public boolean midiKeyboard;

    public ElectronicMusic(int time, boolean computer, boolean midiKeyboard) {
        super(time);
        this.computer = computer;
        this.midiKeyboard = midiKeyboard;
    }

    public ElectronicMusic(ElectronicMusic electronicMusic){
        super(electronicMusic);
        if(electronicMusic != null){
            this.computer = electronicMusic.computer;
            this.midiKeyboard = electronicMusic.midiKeyboard;
        }
    }

    @Override
    public Music clone() {
        return new ElectronicMusic(this);
    }

    public boolean isComputer() {
        return computer;
    }

    public boolean isMidiKeyboard() {
        return midiKeyboard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ElectronicMusic that = (ElectronicMusic) o;
        return computer == that.computer &&
                midiKeyboard == that.midiKeyboard;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), computer, midiKeyboard);
    }
}
