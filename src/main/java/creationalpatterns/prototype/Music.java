package creationalpatterns.prototype;

import java.util.Objects;

public abstract class Music {
    private int time;

    public Music(int time) {
        if(time <= 0){
            throw new IllegalArgumentException("Music can't be less or equal zero");
        }
        this.time = time;
    }

    public Music(Music music){
        if(music != null){
            this.time = music.time;
        }
    }

    public abstract Music clone();

    public int getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Music music = (Music) o;
        return time == music.time;
    }

    @Override
    public int hashCode() {
        return Objects.hash(time);
    }
}
