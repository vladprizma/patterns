package creationalpatterns.prototype;

import java.util.Objects;

public class ClassicMusic extends Music{
    private boolean cello;
    private boolean violin;
    private boolean piano;
    private boolean flute;

    public ClassicMusic(int time, boolean cello, boolean violin, boolean piano, boolean flute) {
        super(time);
        this.cello = cello;
        this.violin = violin;
        this.piano = piano;
        this.flute = flute;
    }

    public ClassicMusic(ClassicMusic classicMusic){
        super(classicMusic);
        if(classicMusic != null){
            this.cello = classicMusic.cello;
            this.violin = classicMusic.violin;
            this.piano = classicMusic.piano;
            this.flute = classicMusic.flute;
        }
    }

    public boolean isCello() {
        return cello;
    }

    public boolean isViolin() {
        return violin;
    }

    public boolean isPiano() {
        return piano;
    }

    public boolean isFlute() {
        return flute;
    }

    @Override
    public Music clone() {
        return new ClassicMusic(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ClassicMusic that = (ClassicMusic) o;
        return cello == that.cello &&
                violin == that.violin &&
                piano == that.piano &&
                flute == that.flute;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cello, violin, piano, flute);
    }
}
