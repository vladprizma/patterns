package creationalpatterns.abstractfabric.LanguageSpeaker;

import creationalpatterns.abstractfabric.Factories.GUIFactory;
import creationalpatterns.abstractfabric.Languages.English.English;
import creationalpatterns.abstractfabric.Languages.Russian.Russian;

//Самая важная часть, отделяет стили/виды/подмножества
public class LanguageSpeaker {
    private English englishWord;
    private Russian russianWord;

    public LanguageSpeaker(GUIFactory factory){
        englishWord = factory.sayEnglishWord();
        russianWord = factory.sayRussianWord();
    }

    public String[] sayWord(){
        return new String[]{englishWord.word(),russianWord.word()};
    }

}