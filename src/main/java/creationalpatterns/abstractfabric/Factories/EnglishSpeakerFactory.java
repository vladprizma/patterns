package creationalpatterns.abstractfabric.Factories;

import creationalpatterns.abstractfabric.Languages.English.English;
import creationalpatterns.abstractfabric.Languages.English.EnglishSpeakerEnglishWord;
import creationalpatterns.abstractfabric.Languages.Russian.EnglishSpeakerRussianWord;
import creationalpatterns.abstractfabric.Languages.Russian.Russian;

public class EnglishSpeakerFactory implements GUIFactory {
    @Override
    public English sayEnglishWord() {
        return new EnglishSpeakerEnglishWord();
    }

    @Override
    public Russian sayRussianWord() {
        return new EnglishSpeakerRussianWord();
    }
}
