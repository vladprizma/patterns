package creationalpatterns.abstractfabric.Factories;

import creationalpatterns.abstractfabric.Languages.English.English;
import creationalpatterns.abstractfabric.Languages.English.RussianSpeakerEnglishWord;
import creationalpatterns.abstractfabric.Languages.Russian.Russian;
import creationalpatterns.abstractfabric.Languages.Russian.RussianSpeakerRussianWord;


public class RussianSpeakerFactory implements GUIFactory {
    @Override
    public English sayEnglishWord() {
        return new RussianSpeakerEnglishWord();
    }

    @Override
    public Russian sayRussianWord() {
        return new RussianSpeakerRussianWord();
    }
}
