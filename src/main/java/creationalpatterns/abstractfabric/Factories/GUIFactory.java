package creationalpatterns.abstractfabric.Factories;

import creationalpatterns.abstractfabric.Languages.English.English;
import creationalpatterns.abstractfabric.Languages.Russian.Russian;

//Данный интерфейс просто обобщает свой вид в одно целое
//(т.е. все из папки Languages в свой вид (Russian или English))

//Что-то (Chair) зависящее от разных версий (Modern, Gothic) и обощено чем-то (Furniture)
//может быть автоматизировано с помощью абстрактной фабрики + безопасно (Разные виды/версии не попадут куда не следует)
public interface GUIFactory {
    English sayEnglishWord();
    Russian sayRussianWord();
}
