package behavioralpatterns.Iterator;

public interface Collection {

    Iterator getIterator();

}
