package behavioralpatterns.Iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DocumentFolder implements Collection{
    private List<String> documents;

    public DocumentFolder(){
        this.documents = new ArrayList<>();
        Collections.addAll(documents, "order", "contract", "shares", "decree");
    }

    public DocumentFolder(List<String> documents){
        this.documents = new ArrayList<>();
        this.documents.addAll(documents);
    }

    @Override
    public Iterator getIterator() {
        return new DocumentIterator();
    }

    private class DocumentIterator implements Iterator{
        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < documents.size();
        }

        @Override
        public Object next() {
            return documents.get(index++);
        }

        @Override
        public void reset() {
            this.index = 0;
        }
    }
}
