package behavioralpatterns.Iterator;

public interface Iterator {

    boolean hasNext();

    Object next();

    void reset();

}
