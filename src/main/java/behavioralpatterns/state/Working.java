package behavioralpatterns.state;

public class Working implements State{
    private String state = "Working";

    @Override
    public String getState() {
        return state;
    }
}
