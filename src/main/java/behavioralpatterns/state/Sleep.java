package behavioralpatterns.state;

public class Sleep implements State{
    private String state = "Sleep";

    @Override
    public String getState() {
        return state;
    }
}
