package behavioralpatterns.state;

public class Computer {
    private State state;
    private boolean canWork;

    public void setState(State state) {
        this.state = state;
    }

    public String startApplication(String application){
        if(!canWork){
            return "Can't start " + application + " while computer is off or sleeping";
        }
        return application + " started";
    }

    public void changeState(){
        if(state.getState().equals("Working")){
            setState(new Sleep());
            canWork = false;
        }else if(state.getState().equals("Sleep")){
            setState(new SwitchedOff());
            canWork = false;
        }else if(state.getState().equals("SwitchedOff")){
            setState(new Working());
            canWork = true;
        }
    }

}
