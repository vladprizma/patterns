package behavioralpatterns.state;

public class SwitchedOff implements State{
    private String state = "SwitchedOff";

    @Override
    public String getState() {
        return state;
    }
}
