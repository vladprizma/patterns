package behavioralpatterns.state;

public interface State {

    String getState();

}
