package behavioralpatterns.command.EraserVolume;

public class EraserVolume {

    public String engineerVolume(){
        return "Инженер записал серийный номер ластика и посмотрел объём в справочнике.";
    }

    public String physicistVolume(){
        return "Физик взял ровно один литр воды, бросил туда ластик и измерил объём вытесненной воды.";
    }

    public String mathematicianVolume(){
        return "Математик достал сантиметр и измерил длину окружности ластика." +
                " Затем он разделил результат на два Пи, чтобы узнать радиус," +
                " возвёл полученное значение в куб, снова разделил на Пи," +
                " потом разделил на три четверти и таким образом получил объём.";
    }


}
