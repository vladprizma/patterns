package behavioralpatterns.command.ProfessionsVolume;

import behavioralpatterns.command.professions.FindVolume;

public class ProfessionsVolume {
    private FindVolume engineer;
    private FindVolume physicist;
    private FindVolume mathematician;

    public ProfessionsVolume(FindVolume engineer, FindVolume physicist, FindVolume mathematician) {
        this.engineer = engineer;
        this.physicist = physicist;
        this.mathematician = mathematician;
    }

    public String engineerFindVolume(){
        return engineer.findVolume();
    }

    public String physicistFindVolume(){
        return physicist.findVolume();
    }

    public String mathematicianFindVolume(){
        return mathematician.findVolume();
    }
}
