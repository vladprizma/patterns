package behavioralpatterns.command.professions;

import behavioralpatterns.command.EraserVolume.EraserVolume;

public class Mathematician implements FindVolume {
    private EraserVolume volume;

    public Mathematician(EraserVolume volume){
        this.volume = volume;
    }

    @Override
    public String findVolume() {
        return volume.mathematicianVolume();
    }
}
