package behavioralpatterns.command.professions;

public interface FindVolume {

    String findVolume();

}
