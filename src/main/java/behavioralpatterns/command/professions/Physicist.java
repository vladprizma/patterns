package behavioralpatterns.command.professions;

import behavioralpatterns.command.EraserVolume.EraserVolume;

public class Physicist implements FindVolume {
    private EraserVolume volume;

    public Physicist(EraserVolume volume){
        this.volume = volume;
    }

    @Override
    public String findVolume() {
        return volume.physicistVolume();
    }
}
