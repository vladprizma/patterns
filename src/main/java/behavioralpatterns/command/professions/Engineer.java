package behavioralpatterns.command.professions;

import behavioralpatterns.command.EraserVolume.EraserVolume;

public class Engineer implements FindVolume {
    private EraserVolume volume;

    public Engineer(EraserVolume volume){
        this.volume = volume;
    }

    @Override
    public String findVolume() {
        return volume.engineerVolume();
    }
}
