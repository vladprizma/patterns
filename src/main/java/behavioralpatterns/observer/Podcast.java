package behavioralpatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Podcast implements Observed{
    private List<Observer> observers = new ArrayList<>();
    private boolean podcast;

    public boolean isPodcast() {
        return podcast;
    }

    public void setPodcast(boolean podcast) {
        this.podcast = podcast;
        notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(Observer observer : observers){
            observer.eventNotify(podcast);
        }
    }
}
