package behavioralpatterns.observer;

public class Listener implements Observer {
    private String login;
    private boolean podcast;

    public Listener(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public boolean isPodcastOnline() {
        return podcast;
    }

    @Override
    public void eventNotify(boolean podcast) {
        this.podcast = podcast;
    }
}
