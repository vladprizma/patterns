package behavioralpatterns.observer;

//Наблюдатель (подписчик)
public interface Observer {

    void eventNotify(boolean podcast);

}
