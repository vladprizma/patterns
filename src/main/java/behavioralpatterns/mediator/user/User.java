package behavioralpatterns.mediator.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User implements Mediator{
    private List<User> friends = new ArrayList<>();
    private Map<User, String> lastMessageWithUser = new HashMap<>();
    private boolean call;
    private String avatar = "photo.png";


    @Override
    public void callUser(User userToCall) {
        if(!call && !userToCall.call) {
            this.call = true;
            userToCall.call = true;
        }
    }

    @Override
    public void dropCall(User userOnCall) {
        if(call && userOnCall.call) {
            this.call = false;
            userOnCall.call = false;
        }
    }

    @Override
    public void sendMessage(User user, String message) {
        lastMessageWithUser.put(user, message);
        user.lastMessageWithUser.put(this, message);
    }

    @Override
    public void setAvatar(String file) {
        this.avatar = file;
    }

    @Override
    public void addFriend(User user) {
        this.friends.add(user);
        user.friends.add(this);
    }

    public List<User> getFriends() {
        return friends;
    }

    public Map<User, String> getLastMessageWithUser() {
        return lastMessageWithUser;
    }

    public boolean isCall() {
        return call;
    }

    public String getAvatar() {
        return avatar;
    }
}
