package behavioralpatterns.mediator.user;

public interface Mediator {

    void callUser(User userToCall);

    void dropCall(User userOnCall);

    void sendMessage(User user, String message);

    void setAvatar(String file);

    void addFriend(User user);


}
