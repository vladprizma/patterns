package behavioralpatterns.visitor;

public class Kitchener implements Job{
    @Override
    public String processFood(Wheat wheat) {
        return "Cooks wheat";
    }

    @Override
    public String processFood(Potato potato) {
        return "Cooks potato";
    }
}
