package behavioralpatterns.visitor;

import java.util.ArrayList;
import java.util.List;

public class Container {
    private Food [] foods;

    public Container(){
        this.foods = new Food[]{
                new Wheat(),
                new Potato()
        };
    }

    public List<String> processFoods(Job job){
        List<String> buf = new ArrayList<>();
        for(Food food : foods){
            buf.add(food.process(job));
        }
        return buf;
    }

}
