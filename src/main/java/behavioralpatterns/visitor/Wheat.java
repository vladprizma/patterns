package behavioralpatterns.visitor;

public class Wheat implements Food{

    @Override
    public String process(Job job) {
        return job.processFood(this);
    }
}
