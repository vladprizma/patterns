package behavioralpatterns.visitor;

public interface Food {

    String process(Job job);

}
