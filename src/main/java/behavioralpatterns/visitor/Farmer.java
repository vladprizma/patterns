package behavioralpatterns.visitor;

public class Farmer implements Job {
    @Override
    public String processFood(Wheat wheat) {
        return "Grows wheat";
    }

    @Override
    public String processFood(Potato potato) {
        return "Grows potato";
    }
}
