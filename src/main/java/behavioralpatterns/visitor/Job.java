package behavioralpatterns.visitor;

public interface Job {

    String processFood(Wheat wheat);

    String processFood(Potato potato);

}
