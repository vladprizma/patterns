package behavioralpatterns.visitor;

public class Potato implements Food{

    @Override
    public String process(Job job) {
        return job.processFood(this);
    }
}
