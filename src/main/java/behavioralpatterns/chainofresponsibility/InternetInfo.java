package behavioralpatterns.chainofresponsibility;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class InternetInfo extends GetInformation {

    public InternetInfo(){
        this.info = new HashSet<>();
        Collections.addAll(info, "Almost everything");
    }

    @Override
    public String check(String infoToCheck) {
        if(info.contains(infoToCheck)){
            return "Internet have information about " + infoToCheck;
        }
        return "Internet doesn't contain information about " + infoToCheck;
    }
}
