package behavioralpatterns.chainofresponsibility;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class FriendsInfo extends GetInformation {

    public FriendsInfo(){
        this.info = new HashSet<>();
        Collections.addAll(info, "games", "news", "food");
    }

    @Override
    public String check(String infoToCheck) {
        if(info.contains(infoToCheck)){
            return "Friends know about " + infoToCheck;
        }
        return "Friends don't know about " + infoToCheck;
    }
}
