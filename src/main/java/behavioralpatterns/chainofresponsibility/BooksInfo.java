package behavioralpatterns.chainofresponsibility;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class BooksInfo extends GetInformation{

    public BooksInfo(){
        this.info = new HashSet<>();
        Collections.addAll(info, "Mathematics", "Philosophy", "Stories");
    }

    @Override
    public String check(String infoToCheck) {
        if(info.contains(infoToCheck)){
            return "Books have information about " + infoToCheck;
        }
        return "Books don't have information about " + infoToCheck;
    }

}
