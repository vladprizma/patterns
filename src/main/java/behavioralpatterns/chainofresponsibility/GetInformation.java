package behavioralpatterns.chainofresponsibility;

import java.util.Set;

public abstract class GetInformation {
    private GetInformation nextInformationSource;
    protected Set<String> info;

    //Сначала распросить друзей
    //Поискать в книге
    //Искать в интернете

    public void setNextInformationSource(GetInformation nextInformationSource) {
        this.nextInformationSource = nextInformationSource;
    }

    public abstract String check(String infoToCheck);

    public String checkNext(String infoToCheck) {
        if (nextInformationSource == null) {
            return "No more sources found";
        }
        return nextInformationSource.check(infoToCheck);
    }

}
