package behavioralpatterns.template;

public abstract class DayTime {

    public String getDayTime() {
        return "Right now is " + dayTime();
    }

    public abstract String dayTime();

}
