package behavioralpatterns.template;

public class Evening extends DayTime {
    @Override
    public String dayTime() {
        return "Evening";
    }
}
