package behavioralpatterns.template;

public class Morning extends DayTime{
    @Override
    public String dayTime() {
        return "Morning";
    }
}
