package behavioralpatterns.strategy;

public class Evaporated implements State{
    private String state = "Evaporated state of water";

    @Override
    public String getState() {
        return state;
    }
}
