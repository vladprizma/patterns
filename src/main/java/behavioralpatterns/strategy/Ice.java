package behavioralpatterns.strategy;

public class Ice implements State {
    private String state = "Ice state of water";

    @Override
    public String getState() {
        return state;
    }
}
