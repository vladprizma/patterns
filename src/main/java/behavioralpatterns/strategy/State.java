package behavioralpatterns.strategy;

public interface State {

    String getState();

}
