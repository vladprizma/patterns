package behavioralpatterns.strategy;

public class Water {
    private State state;
    private boolean canWork;

    public void setState(State state) {
        this.state = state;
    }

    public String getStateInfo(){
        return state.getState();
    }
}
