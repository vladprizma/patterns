package behavioralpatterns.strategy;

public class Liquid implements State{
    private String state = "Liquid state of water";

    @Override
    public String getState() {
        return state;
    }
}
