package behavioralpatterns.memento;

public class Bookmark {
    private String book;
    private int page;

    public Bookmark(String book, int page){
        this.book = book;
        this.page = page;
    }

    public Save save(){
        return new Save(book, page);
    }

    public void load(Save save){
        this.book = save.getBook();
        this.page = save.getPage();
    }

    public String getBook() {
        return book;
    }

    public int getPage() {
        return page;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
