package behavioralpatterns.memento;

public class Save {
    private final String book;
    private final int page;

    public Save(String book, int page) {
        this.book = book;
        this.page = page;
    }

    public String getBook() {
        return book;
    }

    public int getPage() {
        return page;
    }
}
