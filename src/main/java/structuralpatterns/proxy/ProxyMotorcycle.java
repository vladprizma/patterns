package structuralpatterns.proxy;

public class ProxyMotorcycle implements Transport {
    private Motorcycle motorcycle;

    public ProxyMotorcycle(){
        motorcycle = new Motorcycle();
    }

    //Можно сделать проверку на безопасность
    public String integrityCheck(){
        motorcycle.stop();
        return "Motorcycle is intact";
    }

    @Override
    public String drive() {
        return motorcycle.drive();
    }

    @Override
    public String stop() {
        return motorcycle.stop();
    }
}
