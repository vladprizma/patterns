package structuralpatterns.proxy;

public class Motorcycle implements Transport {
    private boolean isDriving;

    public Motorcycle(){}

    @Override
    public String drive() {
        this.isDriving = true;
        return "Motorcycle is driving now";
    }

    @Override
    public String stop() {
        this.isDriving = false;
        return "Motorcycle is stopped now";
    }


}
