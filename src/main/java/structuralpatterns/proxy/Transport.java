package structuralpatterns.proxy;

public interface Transport {

    String drive();

    String stop();

}
