package structuralpatterns.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Human extends BaseAnimals{

    public Human(String habitat, String color) {
        super(habitat, color);
    }

    @Override
    public List<String> getNutrition() {
        ArrayList<String> nutrition = new ArrayList<>();
        Collections.addAll(new ArrayList<>(),"Any meat", "all not poisonous vegetables and fruits");
        return nutrition;
    }

    @Override
    public int getWeight() {
        return 100;
    }

    @Override
    public int getHeight() {
        return 160;
    }
}
