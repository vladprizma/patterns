package structuralpatterns.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Bird extends BaseAnimals{

    public Bird(String habitat, String color) {
        super(habitat, color);
    }

    @Override
    public List<String> getNutrition() {
        ArrayList<String> nutrition = new ArrayList<>();
        Collections.addAll(new ArrayList<>(),"Fish", "Berries", "Seeds");
        return nutrition;
    }

    @Override
    public int getWeight() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 10;
    }
}
