package structuralpatterns.composite;

import java.util.List;

public interface VertebratesAnimals {

    List<String> getNutrition();

    int getWeight();

    int getHeight();

}
