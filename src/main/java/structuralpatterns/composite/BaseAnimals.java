package structuralpatterns.composite;

public abstract class BaseAnimals implements VertebratesAnimals{
    private String habitat;
    private String color;

    public BaseAnimals(String habitat, String color){
        this.habitat = habitat;
        this.color = color;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }


}
