package structuralpatterns.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fish extends BaseAnimals {


    public Fish(String habitat, String color) {
        super(habitat, color);
    }

    @Override
    public List<String> getNutrition() {
        ArrayList<String> nutrition = new ArrayList<>();
        Collections.addAll(new ArrayList<>(),"Insects", "Dried food", "Vegetables");
        return nutrition;    }

    @Override
    public int getWeight() {
        return 1;
    }

    @Override
    public int getHeight() {
        return 10;
    }
}
