package structuralpatterns.flyweight;

public interface Tool {

    void doWork();

    boolean isWork();

}
