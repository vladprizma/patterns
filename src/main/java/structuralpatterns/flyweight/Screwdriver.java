package structuralpatterns.flyweight;

public class Screwdriver implements Tool {
    private boolean busy;

    public Screwdriver(){
        this.busy = false;
    }

    @Override
    public void doWork() {
        this.busy = true;
    }

    @Override
    public boolean isWork() {
        return this.busy;
    }
}
