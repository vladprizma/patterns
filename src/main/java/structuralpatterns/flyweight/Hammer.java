package structuralpatterns.flyweight;

public class Hammer implements Tool {
    private boolean busy;

    public Hammer(){
        this.busy = false;
    }

    @Override
    public void doWork() {
        this.busy = true;
    }

    @Override
    public boolean isWork() {
        return this.busy;
    }
}
