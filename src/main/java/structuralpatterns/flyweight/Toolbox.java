package structuralpatterns.flyweight;

import java.util.HashMap;
import java.util.Map;

public class Toolbox {
    private static Map<String, Tool> toolBox = new HashMap<>();

    public Toolbox(){}

    public static Tool getTool(String selectedTool){
        Tool tool = toolBox.get(selectedTool);

        if(tool == null){
            switch (selectedTool){
                case "hammer":
                    tool = new Hammer();
                    break;
                case "screwdriver":
                    tool = new Screwdriver();
            }
            toolBox.put(selectedTool, tool);
        }

        return tool;

    }

}
