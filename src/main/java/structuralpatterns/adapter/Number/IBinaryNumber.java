package structuralpatterns.adapter.Number;

public interface IBinaryNumber {

    String toString(int number);
    String getBinaryNumber();

}
