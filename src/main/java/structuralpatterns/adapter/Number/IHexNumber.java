package structuralpatterns.adapter.Number;

public interface IHexNumber {

    int toInt();
    String getHexNumber();

}
