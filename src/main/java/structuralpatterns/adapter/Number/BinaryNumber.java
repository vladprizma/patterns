package structuralpatterns.adapter.Number;

//2-е число
public class BinaryNumber implements IBinaryNumber {
    private String binaryNumber;

    public BinaryNumber(String binaryNumber){
        setBinaryNumber(binaryNumber);
    }

    public String toString(int number){
        return String.valueOf(number);
    }

    public String getBinaryNumber() {
        return binaryNumber;
    }

    public void setBinaryNumber(String binaryNumber) {
        if(binaryNumber == null || binaryNumber.equals("")){
            throw new IllegalArgumentException("String is empty");
        }
        this.binaryNumber = binaryNumber;
    }



}
