package structuralpatterns.adapter.Number;

//16-е число
public class HexNumber implements IHexNumber{
    private String hexNumber;

    public HexNumber(String hexNumber){
        setHexNumber(hexNumber);
    }

    public String getHexNumber() {
        return hexNumber;
    }

    public int toInt(){ ;
        return Integer.parseInt(Integer.toString(Integer.parseInt(hexNumber, 16), 10));
    }

    public void setHexNumber(String hexNumber) {
        if(hexNumber == null || hexNumber.equals("")){
            throw new IllegalArgumentException("String is empty");
        }
        this.hexNumber = hexNumber;
    }


}
