package structuralpatterns.adapter;

import structuralpatterns.adapter.Number.BinaryNumber;
import structuralpatterns.adapter.Number.IHexNumber;

//У нас была нужда в методе toInt как и в IHexNumber, но так как его не было в IBinaryNumber,
//мы создали адаптер который и будет отвечать за перевод в такой метод
public class BinaryToDecimalAdapter implements IHexNumber {
    BinaryNumber binaryNumber;

    public BinaryToDecimalAdapter(BinaryNumber binaryNumber) {
        this.binaryNumber = binaryNumber;
    }

    @Override
    public int toInt() {
        return Integer.parseInt(Integer.toString(Integer.parseInt(binaryNumber.getBinaryNumber(), 2), 10));
    }

    @Override
    public String getHexNumber() {
        return null;
    }
}
