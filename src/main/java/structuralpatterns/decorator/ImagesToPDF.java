package structuralpatterns.decorator;

public interface ImagesToPDF {
    //Каждая обертка добавляет какой то формат файла

    void setFile(String fileName);

    String convertToPDF();

}
