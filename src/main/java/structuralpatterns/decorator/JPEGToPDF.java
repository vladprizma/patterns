package structuralpatterns.decorator;

import java.io.File;

public class JPEGToPDF implements ImagesToPDF {
    File image;

    public JPEGToPDF(String fileName){
        setFile(fileName);
    }

    @Override
    public void setFile(String fileName) {
        if(!fileName.substring(fileName.length()-4).equals("jpeg")){
            throw new IllegalArgumentException("Wrong image format");
        }
        this.image = new File(fileName);
    }

    public String getFilename(){
        return image.getName();
    }

    @Override
    public String convertToPDF() {
        StringBuilder pdfFilename = new StringBuilder(this.image.getName());
        pdfFilename.delete(pdfFilename.length()-4, pdfFilename.length());
        pdfFilename.append("pdf");
        return pdfFilename.toString();
    }

}
