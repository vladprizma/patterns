package structuralpatterns.decorator;

import java.io.File;

public class PNGToPDF implements ImagesToPDF{
    private JPEGToPDF jpegConverter;
    private File image;

    public PNGToPDF(String fileName){
        setFile(fileName);
    }

    @Override
    public void setFile(String fileName) {
        if(fileName.substring(fileName.length()-3).equals("png")){
            this.image = new File(fileName);
        }
        else {
            jpegConverter = new JPEGToPDF(fileName);
            this.image = new File(fileName);
        }
    }

    public String getFilename(){
        return image.getName();
    }

    @Override
    public String convertToPDF() {
        if(jpegConverter.getFilename().equals(image.getName())){
            return jpegConverter.convertToPDF();
        }
        else {
            StringBuilder pdfFilename = new StringBuilder(this.image.getName());
            pdfFilename.delete(pdfFilename.length() - 3, pdfFilename.length());
            pdfFilename.append("pdf");
            return pdfFilename.toString();
        }
    }
}
