package structuralpatterns.decorator;

import java.io.File;

public class PSDToPDF implements ImagesToPDF {
    private PNGToPDF pngAndJPEGConverter;
    private File image;

    public PSDToPDF(String fileName){
        setFile(fileName);
    }

    @Override
    public void setFile(String fileName) {
        if(fileName.substring(fileName.length()-3).equals("psd")){
            this.image = new File(fileName);
        }
        else {
            pngAndJPEGConverter = new PNGToPDF(fileName);
            this.image = new File(fileName);
        }
    }

    @Override
    public String convertToPDF() {
        if(pngAndJPEGConverter.getFilename().equals(image.getName())){
            return pngAndJPEGConverter.convertToPDF();
        }
        else {
            StringBuilder pdfFilename = new StringBuilder(this.image.getName());
            pdfFilename.delete(pdfFilename.length() - 3, pdfFilename.length());
            pdfFilename.append("pdf");
            return pdfFilename.toString();
        }
    }
}
