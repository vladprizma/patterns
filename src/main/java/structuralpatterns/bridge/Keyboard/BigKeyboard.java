package structuralpatterns.bridge.Keyboard;

import structuralpatterns.bridge.Computer.Computer;

public class BigKeyboard extends SmallKeyboard{

    public BigKeyboard(Computer computer){
        super.computer = computer;
    }

    public void enable(){
        computer.enable();
    }

    public void finishWork(){
        computer.finishWork();
    }

    public void sleepMode(){
        computer.sleepMode();
    }
}
