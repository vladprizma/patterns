package structuralpatterns.bridge.Keyboard;

public interface Keyboard {


    void setCharacter(char character);

    void deleteCharacter();

    String hotKey(String hotKeyTranslated);

}
