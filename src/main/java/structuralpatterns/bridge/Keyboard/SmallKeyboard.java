package structuralpatterns.bridge.Keyboard;

import structuralpatterns.bridge.Computer.Computer;

public class SmallKeyboard implements Keyboard {
    protected Computer computer;

    public SmallKeyboard(){};

    public SmallKeyboard(Computer computer){
        this.computer = computer;
    }

    @Override
    public void setCharacter(char character) {
        computer.setCharacter(character);
    }

    @Override
    public void deleteCharacter() {
        computer.deleteCharacter();
    }

    @Override
    public String hotKey(String hotKeyTranslated) {
        return computer.hotKey(hotKeyTranslated);
    }
}
