package structuralpatterns.bridge.Computer;

public interface Computer {

    void enable();

    void finishWork();

    void sleepMode();

    void setCharacter(char character);

    String getMemory();

    void deleteCharacter();

    String hotKey(String hotKeyTranslated);

}
