package structuralpatterns.bridge.Computer;

public class PersonalComputer implements Computer {
    private boolean on = false;
    private char lastCharacter;
    private StringBuilder memory;

    @Override
    public void enable() {
        memory = new StringBuilder();
        this.on = true;
    }

    @Override
    public void finishWork() {
        this.memory = null;
        this.on = false;
    }

    @Override
    public void sleepMode() {
        this.on = false;
    }

    @Override
    public void setCharacter(char lastCharacter) {
        this.lastCharacter = lastCharacter;
        this.memory.append(lastCharacter);
    }

    @Override
    public String getMemory() {
        return this.memory.toString();
    }

    @Override
    public void deleteCharacter() {
        this.memory.deleteCharAt(memory.length()-1);
    }

    @Override
    public String hotKey(String hotKeyTranslated) {
        if(hotKeyTranslated.equals("Alt + Enter")){
            return "Fullscreen";
        }
        if(hotKeyTranslated.equals("Alt + F4")){
            return "Close current window";
        }
        return null;
    }
}
