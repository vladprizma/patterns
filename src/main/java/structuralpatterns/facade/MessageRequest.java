package structuralpatterns.facade;

//Отправка запроса на отправление сообщения другому пользователю
public class MessageRequest {

    public MessageRequest(){ }

    //Так же можно провести различные проверки сообщения как и форматирование
    public void messageConnection(User user1, User user2, String message, MessageResponse log){
        log.messageConnection(user2, message + " " + "Sender: " + user1.getLogin());
    }

}
