package structuralpatterns.facade;

import java.util.List;

//Фасад, который упрощает всю систему отправки сообщений
public class Message {
    private MessageResponse log;
    private MessageRequest send;

    public Message(){
        log = new MessageResponse();
        send = new MessageRequest();
    }

    public void sendMessage(User user1, User user2, String message){
        this.send.messageConnection(user1, user2, message, log);
    }

    public List<String> getUserMessages(User user){
        return log.getMessages(user);
    }

}
