package structuralpatterns.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Ответ от сервера о возможности хранить сообщение
public class MessageResponse {
    private Map<String, List<String>> log;

    public MessageResponse(){
        log = new HashMap<>();
    }

    public void messageConnection(User user, String message){
        if(!log.containsKey(user.getLogin())){
            log.put(user.getLogin(), new ArrayList<>());
        }
        log.get(user.getLogin()).add(message);
    }

    public List<String> getMessages(User user){
        if(log.containsKey(user.getLogin())){
            return log.get(user.getLogin());
        }
        return null;
    }

}
