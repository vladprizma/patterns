package creationalpatterns.builder.Director;

import creationalpatterns.builder.Builders.GameBuilder;
import creationalpatterns.builder.Components.Genre;
import creationalpatterns.builder.Components.Speaker;
import creationalpatterns.builder.Components.Style;
import creationalpatterns.builder.Games.Game;
import org.junit.Test;

import static org.junit.Assert.*;

public class DirectorTest {

    @Test
    public void testDirector(){
        Director director = new Director();
        GameBuilder gameBuilder = new GameBuilder();

        director.constructShooter(gameBuilder);
        Game game = gameBuilder.getResult();

        assertEquals(game.getGenre(), Genre.SHOOTER);
        assertEquals(game.getCost(),699);
        assertEquals(game.getDimension(),3);
        assertEquals(game.getStyle(), Style.FUTURISTIC);
        assertEquals(game.getSpeaker(), Speaker.RUSSIAN);
        assertEquals(game.getSubtitles(),"Creator - Vladislav Perevezntsev");

    }

}