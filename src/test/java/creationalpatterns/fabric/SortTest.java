package creationalpatterns.fabric;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortTest {

    @Test
    public void testCountingSort(){
        int [] arr = new int[]{ 1, 5, 7, 8, 4, 2, 9, 3, 3};

        Sort test = new Sort(new SortFactory());
        test.sort(SortFactory.SortTypes.COUNTING, arr);

        int [] arrAnswer = new int[]{ 1, 2, 3, 3, 4, 5, 7, 8, 9};
        assertArrayEquals(arrAnswer, arr);
    }

    @Test
    public void testInsertSort(){
        int [] arr = new int[]{ 1, 5, 7, 8, 4, 2, 9, 3, 3};

        Sort test = new Sort(new SortFactory());
        test.sort(SortFactory.SortTypes.INSERTION, arr);

        int [] arrAnswer = new int[]{ 1, 2, 3, 3, 4, 5, 7, 8, 9};
        assertArrayEquals(arrAnswer, arr);
    }

    @Test
    public void testMergeSort(){
        int [] arr = new int[]{ 1, 5, 7, 8, 4, 2, 9, 3, 3};

        Sort test = new Sort(new SortFactory());
        test.sort(SortFactory.SortTypes.MERGE, arr);

        int [] arrAnswer = new int[]{ 1, 2, 3, 3, 4, 5, 7, 8, 9};
        assertArrayEquals(arrAnswer, arr);
    }

}