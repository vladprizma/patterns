package creationalpatterns.singleton;

import org.junit.Test;

import static org.junit.Assert.*;

public class DiaryTest {

    @Test(expected = IllegalArgumentException.class)
    public void testDiary1(){
        Diary.getDiary().getNote(0);
    }

    @Test
    public void testDiary2(){
        Diary.getDiary().setNote("First note in this diary, my day was very fun");

        assertEquals(Diary.getDiary().getNote(0), "First note in this diary, my day was very fun");
    }

}