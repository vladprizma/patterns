package creationalpatterns.prototype;

import org.junit.Test;

import static org.junit.Assert.*;

public class MusicTest {

    @Test
    public void testMusic(){
        ClassicMusic classicMusic = new ClassicMusic(150,false, true,true,true);

        ClassicMusic copy = (ClassicMusic) classicMusic.clone();
        assertEquals(copy,classicMusic);
    }

}