package creationalpatterns.abstractfabric;

import creationalpatterns.abstractfabric.Factories.RussianSpeakerFactory;
import creationalpatterns.abstractfabric.LanguageSpeaker.LanguageSpeaker;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestLanguageSpeaker {

    @Test
    public void testLanguageSpeaker(){
        LanguageSpeaker speaker;
        RussianSpeakerFactory russianSpeaker = new RussianSpeakerFactory();
        speaker = new LanguageSpeaker(russianSpeaker);

        assertEquals(speaker.sayWord()[0],"Старт");//Английское слово
        assertEquals(speaker.sayWord()[1],"Спутник");//Русское слово
    }
}
