package behavioralpatterns.visitor;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ContainerTest {

    @Test
    public void foodTest(){
        Container container = new Container();
        Farmer farmer = new Farmer();

        List<String> expected1 = new ArrayList<>();
        expected1.add("Grows wheat");
        expected1.add("Grows potato");

        assertEquals(expected1, container.processFoods(farmer));

        Kitchener kitchener = new Kitchener();

        List<String> expected2 = new ArrayList<>();
        expected2.add("Cooks wheat");
        expected2.add("Cooks potato");

        assertEquals(expected2, container.processFoods(kitchener));
    }

}