package behavioralpatterns.strategy;

import org.junit.Test;

import static org.junit.Assert.*;

public class WaterTest {

    @Test
    public void waterStatesTest(){
        Water water = new Water();

        water.setState(new Liquid());
        assertEquals("Liquid state of water", water.getStateInfo());

        water.setState(new Ice());
        assertEquals("Ice state of water", water.getStateInfo());

        water.setState(new Evaporated());
        assertEquals("Evaporated state of water", water.getStateInfo());
    }

}