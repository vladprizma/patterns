package behavioralpatterns.mediator.user;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void userTest(){
        User user1 = new User();
        User user2 = new User();

        user1.sendMessage(user2, "Hello! How are you?");
        assertEquals("Hello! How are you?", user2.getLastMessageWithUser().get(user1));
    }

}