package behavioralpatterns.chainofresponsibility;

import org.junit.Test;

import static org.junit.Assert.*;

public class GetInformationTest {

    @Test
    public void infoTest(){
        FriendsInfo friends = new FriendsInfo();
        BooksInfo books = new BooksInfo();
        InternetInfo internet = new InternetInfo();

        friends.setNextInformationSource(books);
        books.setNextInformationSource(internet);

        assertEquals("Books have information about Philosophy", friends.checkNext("Philosophy"));
    }

}