package behavioralpatterns.state;

import org.junit.Test;

import static org.junit.Assert.*;

public class ComputerTest {

    @Test
    public void computerTest(){
        Computer computer = new Computer();

        computer.setState(new SwitchedOff());
        computer.changeState();

        assertEquals("IntelliJ IDEA started", computer.startApplication("IntelliJ IDEA"));

        computer.changeState();

        assertEquals("Can't start IntelliJ IDEA while computer is off or sleeping", computer.startApplication("IntelliJ IDEA"));

    }
}