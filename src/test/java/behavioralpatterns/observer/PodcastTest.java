package behavioralpatterns.observer;

import org.junit.Test;

import static org.junit.Assert.*;

public class PodcastTest {

    @Test
    public void podcastTest(){
        Podcast podcast = new Podcast();

        Listener listener1 = new Listener("User1");
        Listener listener2 = new Listener("User2");

        podcast.addObserver(listener1);
        podcast.addObserver(listener2);

        podcast.setPodcast(true);

        assertTrue(listener1.isPodcastOnline());
        assertTrue(listener2.isPodcastOnline());

        podcast.setPodcast(false);

        assertFalse(listener1.isPodcastOnline());
        assertFalse(listener2.isPodcastOnline());
    }

}