package behavioralpatterns.memento;

import org.junit.Test;

import static org.junit.Assert.*;

public class BookmarkTest {

    @Test
    public void bookmarkTest(){
        Bookmark bookmark = new Bookmark("Brave new world!", 0);
        bookmark.setPage(10);
        Save save1 = bookmark.save();

        bookmark.setBook("Design patterns");
        bookmark.setPage(100);
        Save save2 = bookmark.save();

        bookmark.load(save1);
        assertEquals("Brave new world!", bookmark.getBook());
        assertEquals(10, bookmark.getPage());

    }

}