package behavioralpatterns.template;

import org.junit.Test;

import static org.junit.Assert.*;

public class DayTimeTest {

    @Test
    public void voidDayTimeTest(){
        Morning morning = new Morning();

        assertEquals("Right now is Morning", morning.getDayTime());

        Evening evening = new Evening();

        assertEquals("Right now is Evening", evening.getDayTime());
    }

}