package behavioralpatterns.Iterator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DocumentFolderTest {

    @Test
    public void documentTest(){
        DocumentFolder documents = new DocumentFolder();

        Iterator iterator = documents.getIterator();

        assertEquals("order", iterator.next());
        assertEquals("contract", iterator.next());
        assertEquals("shares", iterator.next());
        assertEquals("decree", iterator.next());
    }

}