package behavioralpatterns.command;

import behavioralpatterns.command.EraserVolume.EraserVolume;
import behavioralpatterns.command.ProfessionsVolume.ProfessionsVolume;
import behavioralpatterns.command.professions.Engineer;
import behavioralpatterns.command.professions.Mathematician;
import behavioralpatterns.command.professions.Physicist;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProfessionsVolumeTest {

    @Test
    public void findVolumeTest(){
        EraserVolume eraserVolume = new EraserVolume();

        ProfessionsVolume professionsVolume =
                new ProfessionsVolume(new Engineer(eraserVolume),
                        new Physicist(eraserVolume), new Mathematician(eraserVolume));

        assertEquals( "Математик достал сантиметр и измерил длину окружности ластика." +
                " Затем он разделил результат на два Пи, чтобы узнать радиус," +
                " возвёл полученное значение в куб, снова разделил на Пи," +
                " потом разделил на три четверти и таким образом получил объём.", professionsVolume.mathematicianFindVolume());

        assertEquals("Физик взял ровно один литр воды, бросил туда ластик и измерил объём вытесненной воды.",
                professionsVolume.physicistFindVolume());

        assertEquals( "Инженер записал серийный номер ластика и посмотрел объём в справочнике.",
                professionsVolume.engineerFindVolume());
    }

}