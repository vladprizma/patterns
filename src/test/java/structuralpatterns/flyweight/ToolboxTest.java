package structuralpatterns.flyweight;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToolboxTest {

    @Test
    public void toolboxTest(){
        Tool hammer = new Hammer();

        //Даже если будет сотни таких вызовов, затраты будут минимальны, т.к. контролируется создание копий
        Toolbox.getTool("hammer").doWork();
        Toolbox.getTool("hammer").doWork();
        Toolbox.getTool("hammer").doWork();
        Toolbox.getTool("hammer").doWork();
        Toolbox.getTool("hammer").doWork();

        assertEquals(hammer.getClass(), Toolbox.getTool("hammer").getClass());
    }

}