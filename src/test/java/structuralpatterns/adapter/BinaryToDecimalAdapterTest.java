package structuralpatterns.adapter;

import structuralpatterns.adapter.Number.BinaryNumber;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinaryToDecimalAdapterTest {

    @Test
    public void testBinaryToHexAdapter(){
        BinaryToDecimalAdapter binaryToDecimalAdapter = new BinaryToDecimalAdapter(new BinaryNumber("10000111"));

        //10000111 = 135
        assertEquals(135, binaryToDecimalAdapter.toInt());
    }

}