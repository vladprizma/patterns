package structuralpatterns.facade;

import org.junit.Test;

import static org.junit.Assert.*;

public class MessageTest {

    @Test
    public void sendMessageTest(){
        Message message = new Message();
        User user1 = new User("user1", "password123", "John", "Johnov");
        User user2 = new User("user2", "password123", "NotJohn", "Johnov");

        message.sendMessage(user1, user2, "Hi NotJohn");
        assertEquals("Hi NotJohn Sender: user1", message.getUserMessages(user2).get(0));

        message.sendMessage(user2, user1, "Hello John");
        assertEquals("Hello John Sender: user2", message.getUserMessages(user1).get(0));

    }

}