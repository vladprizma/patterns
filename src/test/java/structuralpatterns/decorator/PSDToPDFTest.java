package structuralpatterns.decorator;

import org.junit.Test;

import static org.junit.Assert.*;

public class PSDToPDFTest {

    @Test
    public void psdToPDFTest(){
        PSDToPDF psd = new PSDToPDF("image.jpeg");

        assertEquals("image.pdf", psd.convertToPDF());
    }

}