package structuralpatterns.bridge.Keyboard;

import structuralpatterns.bridge.Computer.PersonalComputer;
import org.junit.Test;

import static org.junit.Assert.*;

public class BigKeyboardTest {

    @Test
    public void bigKeyboardTest(){
        PersonalComputer computer = new PersonalComputer();
        computer.enable();
        BigKeyboard bigKeyboard = new BigKeyboard(computer);

        bigKeyboard.setCharacter('C');
        bigKeyboard.setCharacter('o');
        bigKeyboard.setCharacter('m');
        bigKeyboard.setCharacter('p');
        bigKeyboard.setCharacter('y');
        bigKeyboard.deleteCharacter();
        bigKeyboard.setCharacter('u');
        bigKeyboard.setCharacter('t');
        bigKeyboard.setCharacter('e');
        bigKeyboard.setCharacter('r');

        assertEquals("Computer", computer.getMemory());
    }

}