package structuralpatterns.proxy;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProxyMotorcycleTest {

    @Test
    public void motorcycleProxyTest(){
        ProxyMotorcycle motorcycle = new ProxyMotorcycle();

        assertEquals("Motorcycle is driving now", motorcycle.drive());
        assertEquals("Motorcycle is intact", motorcycle.integrityCheck());
    }

}